## codio-guide-find-replace

A tool for Codio guide authors to search and replace text in all guides content markdown.
It will show you each occurence within its context and ask for confirmation before replacing it.

You can give a regexp without the `/` around it in the search term if you want.

<u>Usage : </u> `codiomr <search> <replace>`

Options : `-d, --directory [dir]` : Root directory to search in (default = `.guides/content/`).

Options : `-e, --extension [ext]` : File extension to search for (default = `.md`).
