#!/usr/bin/env node

var program = require('commander');
var colors = require('colors');
var promptly = require('promptly');
var asyncReplace = require('./async-replace');
var fs = require('fs');
var async = require('async');

var searchTerm = "";
var replaceTerm = "";

var before_length = 30;
var after_length = 30;


program
  .version('0.0.1')
  .arguments('<search> <replace>')
  .option('-d, --directory [dir]', 'Root directory to search in.', '/home/codio/workspace/.guides/content/')
  .option('-e, --extension [ext]', 'File extension to search for.', '.md')
  .action(function(search, replace){
    searchTerm=search;
    replaceTerm=replace;
  });

program.parse(process.argv);

console.log('Searching for: "'+colors.green(searchTerm)+'" replace by: "'+colors.green(replaceTerm)+'" in directory: '+colors.green(program.directory) +" extension: "+colors.green(program.extension));

var searchRegex = new RegExp(searchTerm, "g");


fs.readdir(program.directory, function(err, files) {
  
    files.filter(function(file) { return file.substr(-program.extension.length) === program.extension; });
    
    
    async.eachSeries(files, function(file,callback) {
      var fullfile = program.directory+file;
      
      console.log(colors.yellow("\nFile : "+fullfile));
      
      fs.readFile(fullfile, {'encoding':'utf-8'}, function(err, contents) {
        if (err) throw err;

        doReplace(contents,function (replaced) {
          //console.log(replaced);
          fs.writeFileSync(fullfile, replaced, {'encoding':'utf-8'});
          callback();
        }); 
      });
    });        
});


function doReplace(inString, done) {
  
  asyncReplace(inString, searchRegex , function(match, offset, string, callback){
    var match_length = match.length;
    var before = string.substring(offset-1-before_length, offset);
    var after = string.substring(offset+match_length, offset+match_length+after_length);

    console.log(colors.green("Occurence ::"), before+colors.red(match)+after);
    
    var confirm_prompt = colors.blue('Replace? (')+colors.yellow('y')+colors.blue(' for yes, ')+colors.yellow('n')+colors.blue(' for no)');

    promptly.confirm(confirm_prompt, function (err, value) {

        if (value) {
            callback(null,replaceTerm);
        } else {
            callback(null,match);
        }

    });

  }, function(err, newString){
    done(newString);
  });
  
}








